BTC-BOT
=======

Simple BTC bot that, in the future, will act as a telegram bot for tasks related to bitcoin.

Right now, it simply uses blockchain.info's api to get the current price in BRL and logs it
on a file called bitcoinPrice.txt.

Requirements: PHP5+, Phalcon.

Usage:

`````
php cli.php main bitcoin
 `````