<?php

use Phalcon\Cli\Task;

class MainTask extends Task
{
    public function mainAction()
    {
        echo 'This is the default task and the default action' . PHP_EOL;
    }

    /*
     * Gets current bitcoin price from an API and returns a message
     */
    public function bitcoinAction()
    {
        // We're going to use blockchain.info API
        $api  = 'https://blockchain.info/ticker';
        $btcPrice = json_decode(file_get_contents($api));
        $currentPrice = 0;
        foreach ($btcPrice as $currency => $info) {
            if ($currency === 'BRL') {
                foreach ($info as $type => $detail) {
                    if ($type  === 'last') {
                        $currentPrice = $detail;
                    }
                } 
            } 
        }

        // Logs current price to a file
        $date = new DateTime();
        $file = new SplFileObject('bitcoinPrice.txt', "a");
        $written = $file->fwrite($date->format('[Y-d-m]') . "\tBRL: {$currentPrice}\n");

        if ($written > 0) {
            echo 'Successfully written on bitcoinPrice.txt!';
        } else {
            echo 'Something went wrong. Couldn\'t write to file.';
        } 

    }
}